import React, { useState, useEffect } from "react";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import './Map.css';
import axios from 'axios';
import L from 'leaflet'; // Ajoutez cette ligne pour importer L depuis Leaflet

function Map() {
  const [radars, setRadars] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:3000/api/radar/all')
      .then((response) => {
        setRadars(response.data);
      })
      .catch((error) => {
        console.error('Error fetching radar data:', error);
      });
  }, []);

  return (
    <MapContainer center={[47.0, 2.0]} zoom={6} className="map-container">
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {radars.map((radar) => (
  <Marker
    key={radar.id}
    position={[parseFloat(radar.latitude), parseFloat(radar.longitude)]}
    icon={
      new L.divIcon({
        className: "radar-marker",
      })
    }
  >
    <Popup className="radar-popup">
      <div>
        <h2>{radar.emplacement}</h2>
        <p>Route: {radar.route}</p>
        <p>Vitesse: {radar.vitesse_vehicules_legers_kmh} km/h</p>
      </div>
    </Popup>
  </Marker>
))}
    </MapContainer>
  );
}

export default Map;
